package org.sensornet.iot;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.sensornet.iot.data.SensorData;
import org.sensornet.iot.data.TagData;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * A Sensor is an Internet connected device with access to some type
 * of data that it would like to store periodically in the cloud. Data types include:
 * {@link org.sensornet.iot.data.SensorData SensorData} 
 * {@link org.sensornet.iot.data.TagData TagData}
 */
public abstract class Sensor {
	// Constants
	private static final String SITE_PREFIX = "https://sensornet.org/api/data/";
	
	// Instance variables
	private final Gson gson;
	
	/**
	 * Constructor
	 */
	public Sensor() {
		this.gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).serializeNulls().create();
	}
	
	/**
	 * Get the unique key for the sensor
	 * @return Sensor key
	 */
	public abstract String getSensorKey();
	
	/**
	 * Send sensor data to the cloud
	 * @param sensorData Sensor data to send
	 * @return Whether the sensor data was successfully sent
	 * @throws IOException Input/Output exception is thrown for a variety of reasons
	 */
	public final boolean sendSensorData(SensorData sensorData) throws SensorNetException, IOException {
		sensorData.setSensorKey(this.getSensorKey());
		String requestUrl = "index.php";
		
		return this.sendApiPostRequest(sensorData, requestUrl);
	}
	
	/**
	 * Send location tag data to the cloud
	 * @param tagData Location tag data to send
	 * @return Whether the location tag data was successfully sent
	 * @throws IOException Input/Output exception is thrown for a variety of reasons
	 */
	public final boolean sendTagData(TagData tagData) throws SensorNetException, IOException {
		tagData.setSensorKey(this.getSensorKey());
		String requestUrl = "tag.php";
		
		return this.sendApiPostRequest(tagData, requestUrl);
	}
	
	/**
	 * Send a SensorNet API HTTP POST request
	 * @param data Data to be sent (as a class structure)
	 * @param requestUrl Request URL (relative to the Data API)
	 * @return Whether the data was sent successfully
	 * @throws SensorNetException Exception is thrown if the data is not sent successfully
	 * @throws IOException Input/Output exception is thrown for a variety of reasons
	 */
	private boolean sendApiPostRequest(Object data, String requestUrl)  throws SensorNetException, IOException {
		boolean sentSuccessfully = false;
		String jsonData = "";
		jsonData = this.gson.toJson(data);
		URL url;
		try {
			url = new URL(SITE_PREFIX + requestUrl);
		} catch (MalformedURLException e) {
			throw new SensorNetException("Invalid URL: " + SITE_PREFIX + requestUrl);
		}
		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");
		
		OutputStream os = conn.getOutputStream();
		os.write(jsonData.getBytes());
		os.flush();
		
		switch (conn.getResponseCode()) {
		case HttpURLConnection.HTTP_CREATED:
			sentSuccessfully = true;
			// Success!
			break;
		case HttpURLConnection.HTTP_UNAUTHORIZED:
			throw new SensorNetException("Invalid sensor key (Did you remember to set it?)");
		case HttpURLConnection.HTTP_NOT_FOUND:
			throw new SensorNetException("Sensor not found / Unauthorized");
		default:
			throw new SensorNetException("Unknown error, HTTP Code: " + conn.getResponseCode());	
		}
		
		conn.disconnect();
		
		return sentSuccessfully;
	}
}
package org.sensornet.iot;

/**
 * A SensorNet exception defines an exception that occurs while attempting to
 * save sensor data to the cloud
 */
public class SensorNetException extends Exception {
    
	// Constants
	private static final long serialVersionUID = -6843836304912065951L;

	/**
	 * Constructor
	 * @param message Exception message
	 */
	public SensorNetException(String message) {
        super(message);
    }
}
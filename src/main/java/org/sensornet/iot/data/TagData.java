package org.sensornet.iot.data;

/***
 * TagData is a representation of a specific location along with a name and description
 * that will be sent to the cloud.
 */
public final class TagData {
	
	// Instance variables
	private String sensorKey;
	private Double latitude;
	private Double longitude;
	private String name;
	private String description;
	private String image;

	/**
	 * Get the unique sensor key corresponding to the sensor that this data comes from 
	 * @return Sensor key
	 */
	public String getSensorKey() {
		return sensorKey;
	}

	/**
	 * Set the unique sensor key corresponding to the sensor that this data comes from
	 * @param sensorKey Sensor key
	 */
	public void setSensorKey(String sensorKey) {
		this.sensorKey = sensorKey;
	}

	/**
	 * Get the current latitude of the sensor (decimal degrees)
	 * @return Current latitude
	 */
	public Double getLatitude() {
		return latitude;
	}

	/**
	 * Set the current latitude of the sensor (decimal degrees)
	 * @param latitude Current latitude
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	/**
	 * Get the current longitude of the sensor (decimal degrees)
	 * @return Current longitude
	 */
	public Double getLongitude() {
		return longitude;
	}

	/**
	 * Set the current longitude of the sensor (decimal degrees)
	 * @param longitude Current longitude
	 */
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	/**
	 * Get the name of the tagged location
	 * @return Name of the tagged location
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the name of the tagged location
	 * @param name Name of the tagged location
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get a description of the tagged location
	 * @return Description of the tagged location
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set a description of the tagged location
	 * @param description Description of the tagged location
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Get the image (Base64 encoded) of the tagged location 
	 * @return Image of the tagged location
	 */
	public String getImage() {
		return this.image;
	}
	
	/**
	 * Set the image (Base64 encoded) of the tagged location
	 * @param image Image of the tagged location
	 */
	public void setImage(String image) {
		this.image = image;
	}
}
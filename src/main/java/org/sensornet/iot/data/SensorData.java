package org.sensornet.iot.data;

/***
 * SensorData is a representation of the data that will be sent to the cloud.
 * It comes with default fields for location and battery life, but can easily
 * be extended to include custom data.
 */
public class SensorData {
	
	// Instance variables
	private String sensorKey;
	private Double latitude;
	private Double longitude;
	private Integer batteryLife;

	/**
	 * Get the unique sensor key corresponding to the sensor that this data comes from 
	 * @return Sensor key
	 */
	public String getSensorKey() {
		return sensorKey;
	}

	/**
	 * Set the unique sensor key corresponding to the sensor that this data comes from
	 * @param sensorKey Sensor key
	 */
	public void setSensorKey(String sensorKey) {
		this.sensorKey = sensorKey;
	}

	/**
	 * Get the current latitude of the sensor (decimal degrees)
	 * @return Current latitude
	 */
	public Double getLatitude() {
		return latitude;
	}

	/**
	 * Set the current latitude of the sensor (decimal degrees)
	 * @param latitude Current latitude
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	/**
	 * Get the current longitude of the sensor (decimal degrees)
	 * @return Current longitude
	 */
	public Double getLongitude() {
		return longitude;
	}

	/**
	 * Set the current longitude of the sensor (decimal degrees)
	 * @param longitude Current longitude
	 */
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	/**
	 * Get the current battery life of the sensor (0-100)
	 * @return Current battery life
	 */
	public Integer getBatteryLife() {
		return batteryLife;
	}

	/**
	 * Set the current battery life of the sensor (0-100)
	 * @param batteryLife Current battery life
	 */
	public void setBatteryLife(Integer batteryLife) {
		this.batteryLife = batteryLife;
	}
}
# sensornet-iot

[![build status](https://gitlab.com/jhamlin/sensornet-iot/badges/master/build.svg)](https://gitlab.com/jhamlin/sensornet-iot/commits/master)
[![Bintray](https://img.shields.io/bintray/v/jhamli01/maven/sensornet-iot.svg?maxAge=2592000)](https://bintray.com/jhamli01/maven/sensornet-iot)

## Synopsis

The SensorNet-IoT library provides an extremely simple interface for sending data to SensorNet.org from an IoT device. While at this time only it is only a Java library, the plan is to eventually add support for C#.

## Code Example

Simply extend the Sensor abstract class and then extend the data classes (or use them as is if you don't have custom data to send), then call send! It's that simple.

## Installation

The library is available via Maven or Gradle

### Maven

First, update your Maven settings.xml to include the Bintray repositories
```
<?xml version="1.0" encoding="UTF-8" ?>
<settings xsi:schemaLocation='http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd'
          xmlns='http://maven.apache.org/SETTINGS/1.0.0' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
    
    <profiles>
        <profile>
            <repositories>
                <repository>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>bintray-jhamli01-maven</id>
                    <name>bintray</name>
                    <url>http://dl.bintray.com/jhamli01/maven</url>
                </repository>
            </repositories>
            <pluginRepositories>
                <pluginRepository>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>bintray-jhamli01-maven</id>
                    <name>bintray-plugins</name>
                    <url>http://dl.bintray.com/jhamli01/maven</url>
                </pluginRepository>
            </pluginRepositories>
            <id>bintray</id>
        </profile>
    </profiles>
    <activeProfiles>
        <activeProfile>bintray</activeProfile>
    </activeProfiles>
</settings>
```

Now, you should be able to simply add the dependency to your pom.xml
```
<dependency>
  <groupId>org.sensornet</groupId>
  <artifactId>iot</artifactId>
  <version>{latest.tag.release}</version>
  <type>pom</type>
</dependency>
```

### Gradle

First, add the Bintray repository to your build.gradle
```
repositories {
    maven {
        url  "http://dl.bintray.com/jhamli01/maven" 
    }
}
```

Now, you should be able to simply add the dependency to your build.gradle

```
compile 'org.sensornet:iot:{latest.tag.release}'
```

## API Reference

[Javadoc](https://sensornet.org/api/doc/)

## Contributors

For now it's just me, but I'm open to more contributors!

## License

The MIT License (MIT)
Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
